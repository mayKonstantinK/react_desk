import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

import Popup from './components/popup';
import Desklist from './components/desk-list';
import Header from './components/header';

import { bindActionCreators } from 'redux';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = { userIsActive: this.props.user.isActive };
    }

    render() {
        const isActive = this.props.user.isActive;

        return (
            <div className="app">
                { isActive ? (
                    <div>
                        <Header />
                        <Desklist />
                    </div>
                ) : (
                    <Popup />
                )}
            </div>
        );
    };
};

const mapStateToProps = state => ({
    user: state.user
});

export default connect(mapStateToProps, null)(App);