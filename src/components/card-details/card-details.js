import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { PropTypes } from 'prop-types';

import {
    editCardDescr,
    removeCardDescr,
    renameCard,
    addComment
} from '../../reducers/user';
import Commentlist from '../comment-list';

class Carddetails extends Component {
    constructor(props) {
        super(props);

        this.openEditTitleMode = this.openEditTitleMode.bind(this);
        this.closeEditTitleMode = this.closeEditTitleMode.bind(this);
        this.editCardTitle = this.editCardTitle.bind(this);
        this.onChangeCardTitleEvent = this.onChangeCardTitleEvent.bind(this);
        this.addNewComment = this.addNewComment.bind(this);

        this.openEditDescrMode = this.openEditDescrMode.bind(this);
        this.closeEditDescrMode = this.closeEditDescrMode.bind(this);
        this.editCardDescription = this.editCardDescription.bind(this);
        this.removeCardDescription = this.removeCardDescription.bind(this);
        this.onChangeCardDescrEvent = this.onChangeCardDescrEvent.bind(this);

        this.state = {
            editMode: false,
            editDescrMode: false,
            cardTitle: this.props.card.title,
            cardDescr: this.props.card.description
        };
    }

    openEditTitleMode() {
        this.setState({ editMode: true });
    }

    closeEditTitleMode() {
        this.setState({ editMode: false });
    }

    openEditDescrMode() {
        this.setState({ editDescrMode: true });
    }

    closeEditDescrMode() {
        this.setState({ editDescrMode: false });
    }

    editCardTitle(e) {
        const { desk, card } = this.props,
        title = this.state.cardTitle;

        if (title && title !== ' ') {
            this.props.renameCard(this.state.cardTitle, desk.id, card.id);
            this.closeEditTitleMode();
        }
    }

    editCardDescription(e) {
        const { desk, card } = this.props,
        desc = this.state.cardDescr;
        if (desc && desc !== ' ') {
            this.props.editCardDescr(this.state.cardDescr, desk.id, card.id);
            this.closeEditDescrMode();
        }
    }

    removeCardDescription() {
        const { desk, card } = this.props;

        this.setState({cardDescr: ''});
        this.props.editCardDescr('', desk.id, card.id);
    }

    onChangeCardTitleEvent(e) {
        let inputVal = e.target.value;
        this.setState({cardTitle: inputVal});
    }

    onChangeCardDescrEvent(e) {
        let inputVal = e.target.value;
        this.setState({cardDescr: inputVal});
    }

    addNewComment() {
        const deskId = this.props.desk.id,
              cardId = this.props.card.id,
              message = this.commInput.value,
              author = this.props.user.name;

        if (message && message !== ' ') {
            this.props.addComment(message, deskId, cardId, author);
            this.commInput.value = '';
        }
    }

    render() {
        const { desk, card, comments } = this.props,
              { editMode, editDescrMode } = this.state;

        return (
            <div className="popup-card-block">
                <div className="card-info">
                    <div className="glyphicon glyphicon-tasks"></div>
                    { editMode ? (
                        <div className="card-info__edit-input">
                            <input value = { this.state.cardTitle } type="text" placeholder="Enter the card name..." onChange = { this.onChangeCardTitleEvent } />
                            <div className="glyphicon glyphicon-ok" onClick = { this.editCardTitle }></div>
                            <div className="glyphicon glyphicon-remove-circle" onClick = { this.closeEditTitleMode }></div>
                        </div>
                    ) : (
                        <div className="card-info__card-title" onClick = { this.openEditTitleMode }>{ card.title }</div>
                    )}
                    <div className="card-info__info_item">
                        <div className="card-info__desk-title">Desk: </div>
                        <div className="card-info__desk-body">{ desk.title }</div>
                    </div>
                    <div className="card-info__info_item">
                        <div className="card-info__desk-title">Author: </div>
                        <div className="card-info__desk-body">{ card.author }</div>
                    </div>
                    <div className="descr-block">
                        { editDescrMode ? (
                            <div>
                                <div className="descr-block__descr-title">Description:</div>
                                <textarea className="descr-block__text-area" value = { this.state.cardDescr } type="text" placeholder="Enter the card description..." onChange = { this.onChangeCardDescrEvent } />
                                <div className="glyphicon glyphicon-ok" onClick = { this.editCardDescription }></div>
                                <div className="glyphicon glyphicon-remove-circle" onClick = { this.closeEditDescrMode }></div>
                            </div>
                        ) : (
                            <div>
                                <div className="descr-block__descr-title">Description:</div>
                                <div className="descr-block__descr-body">{ card.description }</div>
                                <div className="descr-block__descr-tools">
                                    <div className="glyphicon glyphicon-pencil" onClick = { this.openEditDescrMode }></div>
                                    <div className="glyphicon glyphicon-remove" onClick = { this.removeCardDescription }></div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
                <div className="card-comments">
                    <div className="glyphicon glyphicon-comment"></div>
                    <div className="card-comments__comm-title">Add comments</div>
                    <div className="card-comments__card-addcomm">
                        <textarea className="card-comments__add-input" type="text" placeholder="Write your comment..." ref = { (input) => { this.commInput = input } } />
                        <br/>
                        <button onClick = { this.addNewComment }>
                            <i class="material-icons">Add comment</i>
                        </button>
                    </div>
                    <hr/>
                    <div className="comments-list">
                        <Commentlist card = { card }
                                     desk = { desk }
                                     comments = { comments }
                        />
                    </div>
                </div>
            </div>
        );
    };
};

Carddetails.propTypes = {
    card: PropTypes.object.isRequired,
    desk: PropTypes.object.isRequired,
    comments: PropTypes.array.isRequired,
    addComment: PropTypes.func.isRequired,
    renameCard: PropTypes.func.isRequired,
    removeCardDescr: PropTypes.func.isRequired,
    editCardDescr: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        editCardDescr,
        removeCardDescr,
        renameCard,
        addComment
    }, dispatch);
  };

export default connect(mapStateToProps, mapDispatchToProps)(Carddetails);