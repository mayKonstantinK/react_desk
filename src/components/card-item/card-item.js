import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal} from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { PropTypes } from 'prop-types';

import {
    removeCard
} from '../../reducers/user';
import Carddetails from '../card-details';

class Carditem extends Component {
    constructor(props) {
        super(props);

        this.state = { showModal: false };
        this.openDetails = this.openDetails.bind(this);
        this.closeDetails = this.closeDetails.bind(this);
        this.removeCard = this.removeCard.bind(this);
    }

    openDetails() {
        this.setState({ showModal: true })
    }

    closeDetails() {
        this.setState({ showModal: false })
    }

    removeCard(e) {
        e.preventDefault();
        const { desk, card } = this.props;
        this.props.removeCard(desk.id, card.id);
    }

    render() {
        const { desk, card, index } = this.props,
        comments = card.comments;

        return (
            <div>
                <div className="card-item" key = { index } onClick = { this.openDetails }>
                    <div className="card-item__card-title" >
                        <div className="card-item__title">{ card.title }</div>
                    </div>
                    <div className="card-item__card-remove" onClick = { this.removeCard }>
                        <span className="glyphicon glyphicon-remove"></span>
                    </div>
                    <br/>
                    <div className="card-item__card-description">
                        { card.description ? (
                            <span className="glyphicon glyphicon-tasks"></span>
                        ) : (
                            <span></span>
                        )}
                        { comments.length ? (
                            <span className="glyphicon glyphicon-comment"> { comments.length } </span>
                        ) : (
                            <span></span>
                        )}
                    </div>
                </div>
                <Modal show = { this.state.showModal } onHide={ this.closeDetails } >
                    <Modal.Body>
                        <div className="popup-close" onClick = { this.closeDetails } >
                            <span className="glyphicon glyphicon-remove"></span>
                        </div>
                        <Carddetails card = { card }
                                     desk = { desk }
                                     comments = { comments }
                        />
                    </Modal.Body>
                </Modal>
            </div>
        );
    };
};

Carditem.propTypes = {
    card: PropTypes.object.isRequired,
    desk: PropTypes.object.isRequired,
    removeCard: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        removeCard
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Carditem);