import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';

import Carditem from '../card-item';

class Cardlist extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { cards, desk } = this.props;

        return (
            <div className="card-list">
                { cards.map((card, index) =>
                    <Carditem card = { card }
                              index = { index }
                              desk = { desk }
                    />
                ) }
            </div>
        );
    };
};

Cardlist.propTypes = {
    cards: PropTypes.array.isRequired,
    desk: PropTypes.object.isRequired
};

export default connect()(Cardlist);