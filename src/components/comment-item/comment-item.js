import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { PropTypes } from 'prop-types';

import {
    editComment,
    removeComment
} from '../../reducers/user';

class Commentitem extends Component {
    constructor(props) {
        super(props);

        this.removeComment = this.removeComment.bind(this);
        this.editComment = this.editComment.bind(this);
        this.openEditMode = this.openEditMode.bind(this);
        this.closeEditMode = this.closeEditMode.bind(this);
        this.onChangeCommentTitleEvent = this.onChangeCommentTitleEvent.bind(this);

        this.state = {
            editMode: false,
            commentTitle: this.props.comment.message,
            commentInitTitle: this.props.comment.message
        }
    }

    openEditMode() {
        this.setState({ editMode: true });
    }

    closeEditMode() {
        this.setState({ editMode: false });
    }

    removeComment() {
        const deskId = this.props.desk.id,
              cardId = this.props.card.id,
              commentId = this.props.comment.id;

        this.props.removeComment(deskId, cardId, commentId);
    }

    editComment(e) {
        const deskId = this.props.desk.id,
              cardId = this.props.card.id,
              commentId = this.props.comment.id,
              currentTitle = this.state.commentTitle,
              initTitle = this.state.commentInitTitle

        if (currentTitle !== '' && currentTitle !== ' ') {
            this.props.editComment(deskId, cardId, commentId, currentTitle);
            this.setState({commentInitTitle: currentTitle});
            this.closeEditMode();
        } else {
            this.props.editComment(deskId, cardId, commentId, initTitle);
            this.setState({commentTitle: initTitle});
            this.closeEditMode();
        }
    }

    onChangeCommentTitleEvent(e) {
        let inputVal = e.target.value;
        this.setState({commentTitle: inputVal});
    }

    render() {
        const  { card, desk, comment, index } = this.props,
        editMode = this.state.editMode;

        return (
            <div className="comment-item" key = { index }>
                <div className="comment-item__comment-author">{ comment.author }</div>
                { editMode ? (
                    <div className="comment-item__comment-message input-edit">
                            <textarea className = "comment-item__comment-input" value = { this.state.commentTitle } type="text" placeholder="Enter the comment name..." onChange = { this.onChangeCommentTitleEvent } />
                            <span className="glyphicon glyphicon-remove-circle" onClick = { this.closeEditMode }></span>
                            <span className="glyphicon glyphicon-ok" onClick = { this.editComment }></span>
                    </div>
                ) : (
                    <div className="comment-item__comment-message">{ comment.message }</div>
                )}
                <span className="glyphicon glyphicon-remove" onClick = { this.removeComment }></span>
                <span className="glyphicon glyphicon-edit" onClick = { this.openEditMode }></span>
            </div>
        );
    };
};

Commentitem.propTypes = {
    comment: PropTypes.object.isRequired,
    card: PropTypes.object.isRequired,
    desk: PropTypes.object.isRequired,
    removeComment: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        editComment,
        removeComment
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Commentitem);