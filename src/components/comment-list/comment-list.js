import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';

import Commentitem from '../comment-item';

class Commentlist extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { comments, user, card, desk } = this.props;

        return (
            <div>
                { comments.map((comment, index) =>
                    <Commentitem comment = { comment }
                                 index = { index }
                                 card = { card }
                                 desk = { desk }
                    />
                ) }
            </div>
        );
    };
};

Commentlist.propTypes = {
    comments: PropTypes.array.isRequired,
    user: PropTypes.object.isRequired,
    card: PropTypes.object.isRequired,
    desk: PropTypes.object.isRequired
};

export default connect()(Commentlist);