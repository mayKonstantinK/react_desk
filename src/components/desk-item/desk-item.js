import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Col } from 'react-bootstrap';
import { PropTypes } from 'prop-types';
import { bindActionCreators } from 'redux';

import {
    addCard,
    editDeskTitle
} from '../../reducers/user';
import Cardlist from '../card-list';

class Deskitem extends Component {
    constructor(props) {
        super(props);

        this.editDeskTitle = this.editDeskTitle.bind(this);
        this.addCard = this.addCard.bind(this);

        this.openEditMode = this.openEditMode.bind(this);
        this.closeEditMode = this.closeEditMode.bind(this);
        this.openAddMode = this.openAddMode.bind(this);
        this.closeAddMode = this.closeAddMode.bind(this);

        this.onChangeDeskTitleEvent = this.onChangeDeskTitleEvent.bind(this);

        this.state = {
            editMode: false,
            addMode: false,
            deskTitle: this.props.desk.title,
            initDeskTitle: this.props.desk.title
        };
    }

    openEditMode() {
        this.setState({ editMode: true });
    }

    closeEditMode() {
        this.setState({ editMode: false });
    }

    openAddMode() {
        this.setState({ addMode: true });
    }

    closeAddMode() {
        this.setState({ addMode: false });
    }

    editDeskTitle(e) {
        const desk = this.props.desk,
              initTitle = this.state.initDeskTitle,
              currentTitle = this.state.deskTitle;

        if (currentTitle !== '' && currentTitle !== ' ') {
            this.props.editDeskTitle(currentTitle, desk.id);
            this.setState({initDeskTitle: currentTitle});
            this.closeEditMode();
        } else {
            this.props.editDeskTitle(initTitle, desk.id);
            this.setState({deskTitle: initTitle});
            this.closeEditMode();
        }
    }

    addCard(e) {
        const desk = this.props.desk,
              value = this.cardTitleInput.value,
              author = this.props.user.name;

        if (value && value !== ' ') {
            this.props.addCard(value, desk.id, author);
            this.cardTitleInput.value = '';
            this.closeAddMode();
        }
    }

    onChangeDeskTitleEvent(e) {
        let inputVal = e.target.value;
        this.setState({deskTitle: inputVal});
    }

    render() {
        const { index, desk } = this.props,
              { editMode, addMode } = this.state,
              cards = desk.cards;

        return (
            <div>
                <Col xs={7} md={4} lg={3}>
                    <div className="desk-block__desk-item" key = { index }>
                        <div className="desk-block__desk-title">
                            { editMode ? (
                                <div className="desk-block__title-edit">
                                    <input className="desk-block__title-input" value = { this.state.deskTitle } type="text" placeholder="Enter the desk name..." onChange = { this.onChangeDeskTitleEvent } />
                                    <span className="glyphicon glyphicon-ok" onClick = { this.editDeskTitle }></span>
                                    <span className="glyphicon glyphicon-remove-circle" onClick = { this.closeEditMode }></span>
                                </div>
                            ) : (
                                <div className="desk-block__title" onClick = { this.openEditMode }>{ desk.title }</div>
                            )}
                        </div>
                        <div className="card-block">
                            <Cardlist cards = { cards } desk = { desk } />
                        </div>
                        <div className="add-block">
                            { addMode ? (
                                    <div className="input-block">
                                        <input className="input-block__add-input" type="text" placeholder="Enter the card name..." ref = { (input) => { this.cardTitleInput = input } } />
                                        <span className="glyphicon glyphicon-ok" onClick = { this.addCard }></span>
                                        <span className="glyphicon glyphicon-remove-circle" onClick = { this.closeAddMode }></span>
                                    </div>
                                ) : (
                                    <button className="add-block__add-button" onClick = { this.openAddMode }>
                                        <i class="material-icons">Add card...</i>
                                    </button>
                            )}
                        </div>
                    </div>
                </Col>
            </div>
        );
    };
};

Deskitem.propTypes = {
    user: PropTypes.object.isRequired,
    desk: PropTypes.object.isRequired,
    editDeskTitle: PropTypes.func.isRequired,
    addCard: PropTypes.func.isRequired
};


const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        addCard,
        editDeskTitle
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Deskitem);