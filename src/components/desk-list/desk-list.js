import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Row } from 'react-bootstrap';
import { PropTypes } from 'prop-types';

import Deskitem from '../desk-item';

class Desklist extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const desks = this.props.user.desks;

        return (
            <div className="desk-block">
                <Grid>
                    <Row className="show-grid">
                        { desks.map((desk, index) =>
                            <Deskitem index = { index } desk = { desk } />
                        ) }
                    </Row>
                </Grid>
            </div>
        );
    };
};

Desklist.propTypes = {
    user: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    user: state.user
});

export default connect(mapStateToProps, null)(Desklist);