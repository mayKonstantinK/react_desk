import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { PropTypes } from 'prop-types';

class Header extends Component {
    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout() {
        localStorage.removeItem('reduxPersist:user');
        window.location.href = '/';
    }

    render() {
        const user = this.props.user;
        return (
            <div className="header">
                  <Grid>
                    <Row className="show-grid">
                        <Col xs={8} md={9} lg={10}>
                            <h1 className="header__title">React Kanban</h1>
                        </Col>
                        <Col xs={4} md={3} lg={2}>
                            <div className="header__info">Logged in as: { user.name } </div>
                            <div className="header__logout">
                                <span className="glyphicon glyphicon-log-out" onClick = { this.logout }></span>
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    };
};

Header.propTypes = {
    user: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    user: state.user
});

export default connect(mapStateToProps, null)(Header);