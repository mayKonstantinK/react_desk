import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Modal } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { PropTypes } from 'prop-types';

import {
    createUser
} from '../../reducers/user';

class Popup extends Component {
    constructor(props) {
        super(props);

        this.state = { showModal: true };
        this.createNewUser = this.createNewUser.bind(this);
        this.close = this.close.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
    }

    createNewUser() {
        const inputVal = this.userInput.value;

        if (inputVal && inputVal !== ' ') {
            this.props.createUser(inputVal);
            this.userInput.value = '';
            this.close();
        }
    }

    onKeyPress(e) {
        if (e.key === 'Enter') {
            this.createNewUser();
        }
    }

    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({ showModal: true });
    }

    render() {
        return (
            <div className="app__modal">
                <Modal show = { this.state.showModal }>
                    <Modal.Header>
                        <Modal.Title>Please enter the Username!</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <input type="text" ref = { (input) => { this.userInput = input } } onKeyPress = { this.onKeyPress } />
                    </Modal.Body>

                    <Modal.Footer>
                        <Button bsStyle="primary" onClick = { this.createNewUser }>Login</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    };
};

Popup.propTypes = {
    user: PropTypes.object.isRequired,
    createUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        createUser
    }, dispatch);
  };

export default connect(mapStateToProps, mapDispatchToProps)(Popup);