import React from 'react';
import { render } from 'react-dom';
import App from './app';
import { Provider } from 'react-redux';

import './app.css';
import store from './store'

render( <Provider store = { store }>
            <App />
        </Provider>, document.getElementById('root'));
