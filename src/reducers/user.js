import Immutable, { setIn, updateIn, update, findIndex, push, get, toJs , fromJS, toJSON } from 'immutable';

export const CREATE_USER = 'CREATE_USER';
export const EDIT_DESK_TITLE = 'EDIT_DESK_TITLE';
export const ADD_CARD = 'ADD_CARD';
export const REMOVE_CARD = 'REMOVE_CARD'
export const RENAME_CARD = 'RENAME_CARD';
export const ADD_COMMENT = 'ADD_COMMENT';
export const REMOVE_COMMENT = 'REMOVE_COMMENT';
export const EDIT_COMMENT = 'EDIT_COMMENT';
export const EDIT_CARD_DESCR = 'EDIT_CARD_DESCR';
export const REMOVE_CARD_DESCR = 'REMOVE_CARD_DESCR';

export const PERSIST_REHYDRATE = 'persist/REHYDRATE';

const initialState = {
    id: 0,
    name: '',
    isActive: false,
    desks: [
        {
            id: 0,
            title: 'ToDo',
            cards: []
        },
        {
            id: 1,
            title: 'InProgress',
            cards: []
        },
        {
            id: 2,
            title: 'Testing',
            cards: []
        },
        {
            id: 3,
            title: 'Done',
            cards: []
        }
    ]
};

export const createUser = (userName) => {
    return (dispatch, getState) => {
        dispatch({
            type: CREATE_USER,
            data: userName
        })
    }
}

export const editDeskTitle = (deskTitle, deskId) => {
    return (dispatch, getState) => {
        dispatch({
            type: EDIT_DESK_TITLE,
            data: { deskTitle, deskId }
        })
    }
}

export const addCard = (cardTitle, deskId, author) => {
    return (dispatch, getState) => {
        dispatch({
            type: ADD_CARD,
            data: { cardTitle, deskId, author }
        })
    }
}

export const renameCard = (cardTitle, deskId, cardId) => {
    return (dispatch, getState) => {
        dispatch({
            type: RENAME_CARD,
            data: { cardTitle, deskId, cardId }
        })
    }
}

export const removeCard = (deskId, cardId) => {
    return (dispatch, getState) => {
        dispatch({
            type: REMOVE_CARD,
            data: {deskId, cardId }
        })
    }
}

export const addComment = (message, deskId, cardId, author) => {
    return (dispatch, getState) => {
        dispatch({
            type: ADD_COMMENT,
            data: { message, deskId, cardId, author }
        })
    }
}

export const removeComment = (deskId, cardId, commentId) => {
    return (dispatch, getState) => {
        dispatch({
            type: REMOVE_COMMENT,
            data: { deskId, cardId, commentId }
        })
    }
}

export const editComment = (deskId, cardId, commentId, message) => {
    return (dispatch, getState) => {
        dispatch({
            type: EDIT_COMMENT,
            data: { deskId, cardId, commentId, message }
        })
    }
}

export const editCardDescr = (description, deskId, cardId) => {
    return (dispatch, getState) => {
        dispatch({
            type: EDIT_CARD_DESCR,
            data: { description, deskId, cardId }
        })
    }
}

export const removeCardDescr = (deskId, cardId) => {
    return (dispatch, getState) => {
        dispatch({
            type: REMOVE_CARD_DESCR,
            data: { deskId, cardId }
        })
    }
}

const ACTION_HANDLERS = {      
    [CREATE_USER]: (state, action) => {
        return { ...state,  id: state.id + 1, name: action.data, isActive: !state.isActive };
    },
    [PERSIST_REHYDRATE]: (state, action) => {
        return { ...state, ...action.payload.user };
    },
    [EDIT_DESK_TITLE]: (state, action) => {
        let currentDesk = state.desks[action.data.deskId],
            desks = state.desks;

        currentDesk.title = action.data.deskTitle;
        desks[action.data.deskId] = currentDesk;

        return { ...state, desks: desks };
    },
    [ADD_CARD]: (state, action) => {
        let immutableState = fromJS(state);

        return immutableState.updateIn(['desks'], (desk) => {
          const deskIndex = immutableState.get('desks').findIndex((desk) => desk.get('id') === action.data.deskId);
          return immutableState.get('desks').update(deskIndex, desk => desk.updateIn(['cards'], (cards) => {
            return cards.push(
                { 
                    id: cards.toJSON().length,
                    title: action.data.cardTitle,
                    description: '',
                    author: action.data.author,
                    comments: []
                }
            );
          }));
        }).toJSON();
    },
    [RENAME_CARD]: (state, action) => {
        let currentDesk = state.desks[action.data.deskId],
            desks = state.desks,
            cards = currentDesk.cards,
            currentCard = cards[action.data.cardId];
       
        currentCard.title = action.data.cardTitle;
        cards[action.data.cardId] = currentCard;
        desks[action.data.deskId] = currentDesk;

        return { ...state, desks: desks };
    },
    [REMOVE_CARD]: (state, action) => {
        let currentDesk = state.desks[action.data.deskId],
            desks = state.desks,
            cards = currentDesk.cards;
       
        let filteredCards = cards.filter((card) => {
            return card.id !== action.data.cardId
        });
        currentDesk.cards = filteredCards;
        desks[action.data.deskId] = currentDesk;

        return { ...state, desks: desks };
    },
    [ADD_COMMENT]: (state, action) => {
        let immutableState = fromJS(state);

        return immutableState.updateIn(['desks'], (desk) => {
          const deskIndex = immutableState.get('desks').findIndex((desk) => desk.get('id') === action.data.deskId);
          return immutableState.get('desks').update(deskIndex, desk => desk.updateIn(['cards'], (cards) => {
            const cardIndex = cards.findIndex((card) => card.get('id') === action.data.cardId);
            return cards.update(cardIndex, card => card.updateIn(['comments'], (comments) => {
                return comments.push(
                    { 
                        id: comments.toJSON().length,
                        message: action.data.message,
                        author: action.data.author
                    }
                )
            }));
          }));
        }).toJSON();
    },
    [REMOVE_COMMENT]: (state, action) => {
        let desks = state.desks,
            currentDesk = state.desks[action.data.deskId],
            cards = currentDesk.cards,
            currentCard = cards[action.data.cardId];

        let filteredComments = currentCard.comments.filter((comment) => {
            return comment.id !== action.data.commentId
        });

        currentCard.comments = filteredComments;
        cards[action.data.cardId] = currentCard;
        desks[action.data.deskId] = currentDesk;

        return { ...state, desks: desks };
    },
    [EDIT_COMMENT]: (state, action) => {
        let desks = state.desks,
            currentDesk = state.desks[action.data.deskId],
            cards = currentDesk.cards,
            currentCard = cards[action.data.cardId],
            currentComment = currentCard.comments[action.data.commentId];

        currentComment.message = action.data.message;
        currentCard.comments[action.data.commentId] = currentComment;
        cards[action.data.cardId] = currentCard;
        desks[action.data.deskId] = currentDesk;

        return { ...state, desks: desks };
    },
    [EDIT_CARD_DESCR]: (state, action) => {
        let currentDesk = state.desks[action.data.deskId],
            desks = state.desks,
            cards = currentDesk.cards,
            currentCard = cards[action.data.cardId];
       
        currentCard.description = action.data.description;

        cards[action.data.cardId] = currentCard;
        desks[action.data.deskId] = currentDesk;

        return { ...state, desks: desks };
    },
    [REMOVE_CARD_DESCR]: (state, action) => {
        let currentDesk = state.desks[action.data.deskId],
            desks = state.desks,
            cards = currentDesk.cards,
            currentCard = cards[action.data.cardId];
       
        currentCard.description = '';
        cards[action.data.cardId] = currentCard;
        desks[action.data.deskId] = currentDesk;

        return { ...state, desks: desks };
    }
}

export default function rootReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type];
    return handler ? handler(state, action) : state;
}